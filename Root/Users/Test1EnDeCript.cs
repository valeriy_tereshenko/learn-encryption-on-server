﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Root.Dao.Cript;

namespace Root.Users
{
    class Test1EnDeCript
    {
        public void Run()
        {
            var st = "test string_";
            var cript = new Cripto();
            var cripted = cript.Encript(st);
            var decripted = cript.Dencript(cripted, cript.Key);

            Console.WriteLine("{0} - {1} - {2}",st, cripted, decripted);
            Console.WriteLine(st==decripted ? "Success" : "Failed");
            Console.WriteLine();

            /*
             * Вывод по тексту:
             * .нужен ключ и таблица IV - их я собрал в одну строку ключа.
             * 
             * .можно работать с потоками:
             * ..сам шифратор работает с потоком
             * ..есть BinaryReader, который может считывать из потока числа
             */
        }
    }
}
