﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Root.Dao.Cript;

namespace Root.Users
{
    class Test5EnDeCriptWithPartialKey
    {
        public void Run()
        {
            var keyG = new Cript5();
            var cript = new Cripto3();
            var key = cript.MakeKey();

            var part = key.Skip(32).Aggregate("", (l, r)=>l+r);
            var newKey = keyG.MakeKey(part);


            var data = "_data string_";

            cript.SetKey(newKey);
            var cripted = cript.Encript(data);
            var uncripted = cript.Decript(cripted);

            Console.WriteLine("{0} - {1} - {2}", data, cripted, uncripted);
        }
    }
}
