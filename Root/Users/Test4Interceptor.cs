﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Autofac.Extras.DynamicProxy2;
using Newtonsoft.Json;
using Root.Dao.Dto;

namespace Root.Users
{
    class Test4Interceptor
    {
        /*
         * Вводная:
         * .собрать интерсептор, который будет шифровать помеченные поля.
         * 
         * Ещё:
         * .пароли разные, т.е. надо их выбирать основываясь на данных.
         * => нужен пассворд провайдер и его указывать в атрибуте.
         * 
         * .а можно ли в алгоритме генераторе устанавливать толко одну часть ключа?
         * => можно Test5EnDeCriptWithPartialKey
         */

        public void Run()
        {
            var context = BuildContext();
            var repo = context.Resolve<Dao.Repo>();

            var test = new Dto1 {Message = "Test message"};


            Console.WriteLine("Add: {0}", JsonConvert.SerializeObject(test));
            test = repo.Add(test);
            Console.WriteLine("Has: {0}", JsonConvert.SerializeObject(test));


            Console.WriteLine();
            repo.Add(new Dto1{Message = "Message 2"});


            Console.WriteLine("{0}Get item", Environment.NewLine);
            test = repo.Get<Dto1>(test.Id);
            Console.WriteLine("Has: {0}", JsonConvert.SerializeObject(test));


            Console.WriteLine("{0}Get all", Environment.NewLine);
            var all = repo.GetAll<Dto1>();
            Console.WriteLine("Has: {0}", JsonConvert.SerializeObject(all));
        }

        private IContainer BuildContext()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<Dao.Interceptors.Simply>()
                .SingleInstance();
            builder.RegisterType<Dao.PasswordProviders.ConstPass>();

            builder.RegisterType<Dao.Repo>()
                .SingleInstance()
                .EnableClassInterceptors()
                .InterceptedBy(typeof(Dao.Interceptors.Simply));

            return builder.Build();
        }

        /*
         * итого:
         * .интерсептор
         * .кастомные пассворд провайдеры
         * .шифрование/разбор элементов и списков
         * 
         * может:
         * .упростить работу с массивами? - напрямую проверять какого типа результат вызова
         * .- если IEnumerable<BaseEntity> - то его подменять своим лази линкью.
         * 
         * далее, в основном тесты:
         * .несколько потомков BaseEntity
         * .разные PasswordProvider-ы
         * ! проверяю только на уровень ниже списков - не уверен надо-ли глубже.
         */
    }
}
