﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Root.Dao.Cript;

namespace Root.Users
{
    class Test3EnDeCript
    {
        public void Run()
        {
            var cript = new Cripto3();
            var key = cript.MakeKey();
            var data = "_data string_";

            cript.SetKey(key);
            var cripted = cript.Encript(data);
            var uncripted = cript.Decript(cripted);

            Console.WriteLine("{0} - {1} - {2}", data, cripted, uncripted);

        }
    }
}
