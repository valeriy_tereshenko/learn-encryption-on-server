﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Root.Dao.Cript;

namespace Root.Users
{
    class Test2EnDeCript
    {
        /*
         * Надо перейти к потокам:
         * .чтение запись BinaryReader-ом/BinaryWriter-ом - для int|double
         * .попробовать чтение запись ключа через поток... - хотя они-же короткие...
         * .перегруженные методы Cript2 для строки|int|double
         */

        public void Run()
        {
            var cript = new Cript2();
            var key = cript.MakeKey();
            var data = "_data string_";

            cript.SetKey(key);
            var cripted = cript.Encript(data);
            var uncripted = cript.Decript(cripted);

            Console.WriteLine("{0} - {1} - {2}", data, cripted, uncripted);
        }
        /*
         * Итого:
         * .потоки странно читаються - не получилось добиться считывание текста один к одному в виде байт
         * .алгоритм расчитан на блоки фиксированной длинны - 128 бит = 16 байт = int64 / double
         */
    }
}
