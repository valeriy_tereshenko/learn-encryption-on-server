﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Root.Dao.Dto;

namespace Root.Dao
{
    public class Repo
    {
        private Dictionary<string, BaseEntity> _entities = new Dictionary<string, BaseEntity>();


        public virtual T Add<T>(T item) where T: BaseEntity
        {
            var data = Copy(item);
            data.Id = Guid.NewGuid().ToString();
            Console.WriteLine("Repo.Add: {0}", JsonConvert.SerializeObject(data));

            _entities.Add(data.Id, data);

            return Copy(data);
        }

        public virtual T Get<T>(string id) where T: BaseEntity
        {
            BaseEntity item;
            if (!_entities.TryGetValue(id, out item)) return null;

            Console.WriteLine("Repo.Get: {0}", JsonConvert.SerializeObject(item));
            return Copy(item as T);
        }

        public virtual IEnumerable<T> GetAll<T>()
        {
            var copy = JsonConvert.SerializeObject(_entities.Values);
            return JsonConvert.DeserializeObject<IEnumerable<T>>(copy);
        }

        private T Copy<T>(T item)
        {
            return JsonConvert.DeserializeObject<T>(JsonConvert.SerializeObject(item));
        }
    }
}
