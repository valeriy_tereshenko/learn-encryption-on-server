﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Root.Dao.PasswordProviders;

namespace Root.Dao
{
    [AttributeUsage(AttributeTargets.Property)]
    class CriptAttribute: Attribute
    {
        public Type PasswordProviderType { get; private set; }

        public CriptAttribute(Type passwordProviderType)
        {
            PasswordProviderType = passwordProviderType;
        }
    }
}
