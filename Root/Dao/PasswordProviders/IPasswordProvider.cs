﻿namespace Root.Dao.PasswordProviders
{
    public interface IPasswordProvider
    {
        string GetPassword(object obj);
    }
}