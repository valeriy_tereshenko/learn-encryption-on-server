﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using Castle.Components.DictionaryAdapter.Xml;
using Castle.Core.Internal;
using Castle.DynamicProxy;
using Root.Dao.Cript;
using Root.Dao.Dto;
using Root.Dao.PasswordProviders;

namespace Root.Dao.Interceptors
{
    public class Simply : IInterceptor
    {
        private const string KeyPart = "¤Sç}?HKÄ±ZÌJ";
        private Cript4 _cript;
        private IComponentContext _container;

        public Simply(IComponentContext container)
        {
            _container = container;
        }

        public void Intercept(IInvocation invocation)
        {
            _cript = new Cript4();
            
            //throw new NotImplementedException();
            var entities = SearchForObjectsToEncript(invocation.Arguments);
            Cript(entities);

            invocation.Proceed();

            entities = SearchForObjectsToEncript(new[] {invocation.ReturnValue});
            Cript(entities, doEncript:false);
            /*
             * варианты:
             * .удаление - есть вход (ид), нет выхода.
             * .обновление - есть вход и выход.
             * .получение - 
             * 
             * алгоритм:
             * .проверить входные параметры:
             * ..есть наследник BaseEntity
             * ..есть массив - проверить элементы
             * 
             * .если нашлось что, зашифровать.
             * 
             * .вызвать метод.
             * 
             * .проверить возвращаемое значение:
             * ..объект класса BaseEntity
             * ..массив/перечисление
             */
        }


        private IEnumerable<BaseEntity> SearchForObjectsToEncript(object[] arguments)
        {
            var flat = new LinkedList<object>() as IEnumerable<object>;
            var objects = new LinkedList<object>();

            arguments.ForEach(x =>
            {
                if (x is IEnumerable<object>)
                    flat = flat.Concat(x as IEnumerable<object>);
                else
                    objects.AddLast(x);
            });
            flat = flat.Concat(objects);

            var list = flat
                .Where(x => x is BaseEntity)
                .Cast<BaseEntity>();

            //TODO: развернуть списки, если есть в аргументах

            return list;
        }

        private void Cript(IEnumerable<BaseEntity> objects, bool doEncript = true)
        {
            objects.ForEach(obj =>
            {
                var toEncript = obj.GetType().GetProperties()
                    .Where(prop => prop.HasAttribute<CriptAttribute>())
                    .Select(prop => new {prop, attr = prop.GetAttribute<CriptAttribute>()});

                toEncript.ForEach(pair =>
                {
                    var value = pair.prop.GetValue(obj) as string;
                    var passwordProvider = _container.Resolve(pair.attr.PasswordProviderType) as IPasswordProvider;
                    var key = passwordProvider.GetPassword(value);

                    _cript.SetKey(key);
                    var encripted = doEncript
                        ? _cript.Encript(value)
                        : _cript.Decript(value);

                    pair.prop.SetValue(obj, encripted);
                });
            });
        }
    }
}
