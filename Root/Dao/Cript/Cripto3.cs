﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Root.Dao.Cript
{
    class Cripto3
    {
        private Byte[] _key;
        private Byte[] _iv;

        #region interface

        public Cripto3 SetKey(string key)
        {
            var bytes = StringToBytes(key);

            _key = bytes.Take(32).ToArray();
            _iv = bytes.Skip(32).ToArray();

            return this;
        }

        public string MakeKey()
        {
            using (var myAes = Aes.Create())
            {
                return BytesToString(
                    myAes.Key.ToList()
                        .Concat(myAes.IV.ToList())
                    );
            }
        }


        public int Encript(int data)
        {
            throw new NotImplementedException();
        }
        public int Decript(int data)
        {
            throw new NotImplementedException();
        }


        public double Encript(double data)
        {
            var bytes = BitConverter.GetBytes(data);
            var enctipt = EncryptStringToBytes_Aes(bytes);
            return BitConverter.ToDouble(enctipt, 0);
        }
        public double Decript(double data)
        {
            throw new NotImplementedException();
        }


        public string Encript(string data)
        {
            var bytes = StringToBytes(data).ToArray();
            var encripted = EncryptStringToBytes_Aes(bytes);

            return BytesToString(encripted);
        }
        public string Decript(string data)
        {
            var bytes = StringToBytes(data).ToArray();
            var encripted = DecryptStringFromBytes_Aes(bytes);

            return BytesToString(encripted);
        }
        #endregion


        #region convertors

        private static string BytesToString(IEnumerable<Byte> data)
        {
            var st = data
                .Aggregate(new StringBuilder(), (sb, next) => sb.Append((char)next))
                .ToString();

            return st;
        }

        private static List<Byte> StringToBytes(string data)
        {
            var bytes = data
                .Select(Convert.ToByte)
                .ToList();

            return bytes;
        }

        #endregion


        #region cripto

        private byte[] EncryptStringToBytes_Aes(byte[] plainText)
        {
            
            byte[] encrypted;
            // Create an Aes object
            // with the specified key and IV.
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = _key;
                aesAlg.IV = _iv;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption.
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    using (var swEncrypt = new BinaryWriter(csEncrypt))
                    {

                        //Write all data to the stream.
                        swEncrypt.Write(plainText);
                    }
                    encrypted = msEncrypt.ToArray();
                }
            }

            // Return the encrypted bytes from the memory stream.
            return encrypted;

        }

        private byte[] DecryptStringFromBytes_Aes(byte[] cipherText)
        {
            // Declare the string used to hold
            // the decrypted text.

            // Create an Aes object
            // with the specified key and IV.
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = _key;
                aesAlg.IV = _iv;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption.
                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                using (var srDecrypt = new BinaryReader(csDecrypt))
                {
                    // Read the decrypted bytes from the decrypting stream
                    // and place them in a string.
                    return srDecrypt.ReadBytes((int) msDecrypt.Length);
                }
            }
        }

        #endregion
    }
}
