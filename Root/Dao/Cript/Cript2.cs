﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Root.Dao.Cript
{
    class Cript2
    {
        private Byte[] _key;
        private Byte[] _iv;

        #region interface

        public Cript2 SetKey(string key)
        {
            var bytes = StringToBytes(key);

            _key = bytes.Take(32).ToArray();
            _iv = bytes.Skip(32).ToArray();

            return this;
        }

        public string MakeKey()
        {
            using (var myAes = Aes.Create())
                return BytesToString(
                    myAes.Key.ToList()
                        .Concat(myAes.IV.ToList())
                    );
        }


        public int Encript(int data)
        {
            throw new NotImplementedException();
        }
        public int Decript(int data)
        {
            throw new NotImplementedException();
        }


        public double Encript(double data)
        {
            throw new NotImplementedException();
        }
        public double Decript(double data)
        {
            throw new NotImplementedException();
        }


        public string Encript(string data)
        {
            return Cript(data, doEncrypt: true);
        }
        public string Decript(string data)
        {
            return Cript(data);
        }
        #endregion



        #region crypto helpers

        private void Encrypt_Aes(Action<StreamWriter> write, Action<MemoryStream> read)
        {
            using (Aes aesAlg = Aes.Create())
            {
                aesAlg.Key = _key;
                aesAlg.IV = _iv;

                // Create a decrytor to perform the stream transform.
                var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for encryption.
                using (var msEncrypt = new MemoryStream())
                using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    using (var swEncrypt = new StreamWriter(csEncrypt))
                        write(swEncrypt);

                    var encripted = msEncrypt.ToArray();
                    using (var toRead = new MemoryStream(encripted))
                        read(toRead);
                }
            }
        }

        private void Decrypt_Aes(Action<StreamWriter> write, Action<CryptoStream> read)
        {

            using (var aesAlg = Aes.Create())
            {
                aesAlg.Key = _key;
                aesAlg.IV = _iv;

                // Create a decrytor to perform the stream transform.
                var decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                // Create the streams used for decryption.
                using (var msDecrypt = new MemoryStream())
                {
                    var writer = new StreamWriter(msDecrypt);
                    write(writer);
                    writer.Flush();
                    var tmp = msDecrypt.ToArray();
                    msDecrypt.Position = 0; 

                    using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        //using(var stream = new StreamReader(csDecrypt))
                        read(csDecrypt);
                    }

                }
            }
        }


        #endregion


        #region convertors

        private static string BytesToString(IEnumerable<Byte> data)
        {
            var st = data
                .Aggregate(new StringBuilder(), (sb, next) => sb.Append((char)next))
                .ToString();

            return st;
        }

        private static List<Byte> StringToBytes(string data)
        {
            var bytes = data
                .Select(Convert.ToByte)
                .ToList();

            return bytes;
        }

        #endregion


        #region method wrappers

        private string Cript(string data, bool doEncrypt = false)
        {
            var result = "";


            var writeAction = new Action<StreamWriter>(
                writer => writer.Write(data));

            var readAction = new Action<Stream>(stream =>
            {
                using (var reader = new StreamReader(stream, new UTF8Encoding()))
                    result = reader.ReadToEnd();
            });


            if(doEncrypt)
                Encrypt_Aes(writeAction, readAction);
            else
                Decrypt_Aes(writeAction, readAction);


            return result;
        }
        #endregion
    }
}
