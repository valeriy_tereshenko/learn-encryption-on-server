﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Root.Dao.Cript
{
    class Cript5
    {
        public string MakeKey(string part)
        {
            var partBytes = StringToBytes(part);

            using (var myAes = Aes.Create())
            {
                myAes.IV = partBytes.ToArray();

                return BytesToString(
                    myAes.Key.ToList()
                        .Concat(myAes.IV.ToList())
                    );
            }
        }


        #region convertors

        private static string BytesToString(IEnumerable<Byte> data)
        {
            var st = data
                .Aggregate(new StringBuilder(), (sb, next) => sb.Append((char)next))
                .ToString();

            return st;
        }

        private static List<Byte> StringToBytes(string data)
        {
            var bytes = data
                .Select(Convert.ToByte)
                .ToList();

            return bytes;
        }

        #endregion
    }
}
